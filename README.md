# README #

### What is this repository for? ###

* Interactive space time fabric simulation
* 1.0.0

### How do I get set up? ###

* Download/Clone the repo
* To open the app right click the html file and open it in your browser.
* Comment out orbitControl() to fix the view.  More details in the comments of the code. 